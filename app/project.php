<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class project extends Model
{
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function contents()
    {
        return $this->hasMany('App\content');
    }

}
