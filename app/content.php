<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class content extends Model
{
    protected $fillable = array('image', 'description');

    public function by(User $user)
    {
        $this->user_id = $user->id;
    }

    public function project()
    {
        return $this->belongsTo('App\projects');
    }

    public function user()
    {
        return $this->belongsTo('App\users');
    }
}
