<?php

Route::auth();
//Route::get('/register', 'ProjectsController@index');

//Homepage
Route::get('/', 'HomeController@index');

Route::get('/about', 'HomeController@about');

Route::get('/contact', 'HomeController@contact');

//Projects endpoint
Route::get('projects', 'ProjectsController@index');

Route::get('projects/create', 'ProjectsController@create');

Route::post('projects/store', 'ProjectsController@store');

Route::get('projects/{singleProject}', 'ProjectsController@show');

Route::post('projects/{singleProject}/edit', 'ProjectsController@edit');

Route::put('projects/{singleProject}', 'ProjectsController@update');

Route::delete('projects/{singleProject}', 'ProjectsController@destroy');

//Projects Content endpoint
Route::get('projects/{singleProject}/content', 'ProjectsController@content');

Route::post('content/{singleProject}/store', 'ContentController@store');

Route::get('content/{content}/edit', 'ContentController@edit');

Route::patch('content/{content}', 'ContentController@update');
