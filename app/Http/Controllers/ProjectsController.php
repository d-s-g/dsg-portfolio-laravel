<?php

namespace App\Http\Controllers;

use App\project;
use App\content;
use App\user;
use Illuminate\Http\Request;

class ProjectsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', ['only' => ['create', 'store', 'content']]);
    }

    public function index()
    {
        $projects = Project::all();

        return view('projects.index', compact('projects'));
    }

    public function create(Project $singleProject)
    {
        return view('projects.create', compact('singleProject'));
    }

    public function store(Request $request)
    {
        $singleProject = new Project;

        $singleProject->title   = $request->title;
        $singleProject->slug    = $request->slug;
        $singleProject->banner  = $request->banner;
        $singleProject->snippit = $request->snippit;

        $singleProject->save();

        return redirect()->to('/');

    }

    public function show(Project $singleProject)
    {
        $singleProject->load('contents.user');
        $contentHeader = 'content-header';

        return view('projects.show', compact('singleProject', 'contentHeader'));
    }

    public function content(Project $singleProject)
    {
        $singleProject->load('contents.user');

        return view('content.create', compact('singleProject'));
    }

}
