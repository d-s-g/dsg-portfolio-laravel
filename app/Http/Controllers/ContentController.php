<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\project;
use App\content;
use App\Http\Requests;


class ContentController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request, Project $singleProject)
    {
        $this->validate($request, [
            'description' => 'required'
        ]);
        $content = new Content;

        $content->image       = $request->image;
        $content->description = $request->description;
        $content->user_id = 1;

        $singleProject->contents()->save($content);

        return redirect()->to("/projects/{$singleProject->slug}");

    }

    public function edit(Project $singleProject, Content $content)
    {

        return view('content.edit', compact('singleProject', 'content'));

    }

    public function update(Request $request, Content $content)
    {

        $content->update($request->all());

        return redirect('/');

    }

}
