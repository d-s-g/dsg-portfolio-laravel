<?php

namespace App\Http\Controllers;

class HomeController extends Controller
{

    public function index()
    {
        $homeLinks = ['About', 'Projects', 'Contact'];

        return view('home.index', compact('homeLinks'));
    }

    public function about()
    {

        return view('home.about');

    }

    public function contact()
    {

        return view('home.contact');

    }

}
