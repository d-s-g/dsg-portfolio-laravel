//This listens for a click on homepage then adds the class to the content dom.

const HomePageToHeader = function() {

    let homeNavSelectors = $('[id^=js-home-]'),
        contentNav       = $('#js-header-slide-trigger');

    if (contentNav) {

        let storedClickState = localStorage.getItem('homePageToHeaderTrigger');
        contentNav.addClass(storedClickState);

        if (contentNav.hasClass(storedClickState)) {

            localStorage.clear();

        } else {

            contentNav.css('height', '15rem');

        }

    }

    homeNavSelectors.on('click', function () {

        let saveClickState = localStorage.setItem('homePageToHeaderTrigger', 'slideToHeader');
        return saveClickState;

    });

};

export {HomePageToHeader};