@extends('layouts.layout')

@section('content')

    <body id="home">
        <main class="container">

            <nav class="row">

                <div id="nav-mobile" class="left">
                    <h1>DSG</h1>
                </div>

                    @foreach ($homeLinks as $link)

                        <ul id="nav-mobile" class="right">

                            <li><a id="js-home-{{ strtolower($link) }}"
                                   href="{{ url(strtolower($link)) }}">{{ $link }}</a></li>
                        </ul>

                    @endforeach

            </nav>

        </main>
    </body>

@stop