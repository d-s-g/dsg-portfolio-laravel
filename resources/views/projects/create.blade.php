@extends('layouts.layout')


@section('content')
    <form method="POST" action="/projects/store">
       <div class="row">
           <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
           <div class="input-field">
               <label type="text" for="title">Project Title</label>
               <input name="title"></input>
           </div>
           <div class="input-field">
               <label type="text" for="slug">Url Slug</label>
               <input name="slug"></input>
           </div>
           <div class="input-field">
               <label type="text" for="banner">Banner Image</label>
               <input name="banner"></input>
           </div>
           <div class="input-field">
               <label for="snippit">Snippit</label>
               <textarea type="text" name="snippit"></textarea>
           </div>
            <button class="waves-effect waves-light btn">Submit</button>
       </div>
    </form>
@stop