@extends('layouts.layout')

@include('layouts.header')

@section('content')

    <div class="container">
        <div id="single-content-card" class="card">
            <div class="card-image waves-effect waves-block waves-light">
                <img class="activator" src="http://placehold.it/300x300">
            </div>
            <div class="card-content">
                <span class="card-title activator grey-text text-darken-4"><h3>{{ $singleProject->title }}</h3>
                    <i class="material-icons right">more_vert</i></span>
                <p><a href="#">This is a link</a></p>
            </div>
            <div class="card-reveal">
                <span class="card-title grey-text text-darken-4">Card Title<i class="material-icons right">close</i></span>
                <p>Here is some more information about this product that is only revealed once clicked on.</p>
            </div>
        </div>
    </div>

    @foreach ($singleProject->contents as $content)

        <h5>{{ $content->description }}</h5><a href="/content/{{$content->id}}/edit">Edit this content</a>

    @endforeach
@stop