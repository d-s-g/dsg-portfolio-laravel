@extends('layouts.layout')

@include('layouts.header')

@section('content')

    <div class="container slideInUpContent">

    <h1>Projects</h1>

        @foreach($projects as $project)
            <div class="col s12 m6">
                <div class="card large">
                    <div class="card-image">
                        <a href="{{ url('/projects/' . $project->slug) }}">
                            <img src="http://placehold.it/300x300">
                        </a>
                        <span class="card-title">Card Title</span>
                    </div>
                    <div class="card-content">
                        <p>I am a very simple card. I am good at containing small bits of information.
                            I am convenient because I require little markup to use effectively.</p>
                    </div>
                    <div class="card-action">
                        <a href="#">This is a link</a>
                    </div>
                </div>
            </div>
        @endforeach

    </div>
@stop
