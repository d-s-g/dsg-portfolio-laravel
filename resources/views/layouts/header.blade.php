<header>
    <nav id="js-header-slide-trigger">

        <div class="nav-wrapper {{ isset($contentHeader) ? $contentHeader : false }}">

            <!-- Branding Image -->
            <div id="nav-mobile" class="left">
                <a class="navbar-brand" href="{{ url('/') }}">
                DSG
                </a>
            </div>

            <!-- Right Side Of Navbar -->
            <ul id="nav-mobile" class="right">
                <li><a href="{{ url('/about') }}">About</a></li>
                <li><a href="{{ url('/projects') }}">Projects</a></li>
                <li><a href="{{ url('/contact') }}">Contact</a></li>
                @if (Auth::guest())
                    {{--<li><a href="{{ url('/login') }}">Login</a></li>--}}
                    {{--<li><a href="{{ url('/register') }}">Register</a></li>--}}
                @else
                    <li><a href="#">{{ Auth::user()->name }}</a></li>
                    <li><a href="{{ url('/logout') }}">Logout</a></li>
                @endif

            </ul>

        </div>
    </nav>
</header>