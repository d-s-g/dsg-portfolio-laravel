<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/html">
<head>
	<title></title>

	<link rel="stylesheet" href="{{ asset('materialize-css/css/materialize.css') }}">
	<link rel="stylesheet" href="{{ !(App::isLocal()) ? elixir('css/app.css') : asset('css/app.css') }}">

</head>

	<body>

		@yield('content')

		<script src="{{ asset('js/jquery/jquery-3.0.0.js') }}"></script>
		<script src="{{ asset('js/main.js') }}"></script>
		<script src="{{ asset('materialize-css/js/materialize.js') }}"></script>

	</body>

</html>