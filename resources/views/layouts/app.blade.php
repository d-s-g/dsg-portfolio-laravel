<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <link rel="stylesheet" href="{{ asset('materialize-css/css/materialize.css') }}">
    <link rel="stylesheet" href="{{ elixir('css/app.css') }}">

</head>

    @include('layouts.header')

        <body>

        <div class="container">
            @yield('content')
        </div>

    <!-- JavaScripts -->
    <script src="{{ asset('js/jquery/jquery-3.0.0.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('materialize-css/js/materialize.js') }}"></script>

    </body>

</html>
