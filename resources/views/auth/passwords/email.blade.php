@extends('layouts.app')

<!-- Main Content -->
@section('content')


<div class="row">
    <div class="col s12">

        <div>
            <h3>Reset Password</h3>
        </div>

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <div class="row">

            <form class="col s12" role="form" method="POST" action="{{ url('/login') }}">
                {{ csrf_field() }}

                <div class="row">
                    <div class="input-field col s12 {{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email">E-Mail Address</label>

                        <input id="email" type="email" class="validate" name="email">

                        @if ($errors->has('email'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s6">
                        <button class="btn waves-effect waves-light" type="submit" name="action">
                            Send Password Reset Link
                        </button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>

@endsection
