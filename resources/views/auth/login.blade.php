@extends('layouts.app')

@section('content')
    <div class="container">

    <div class="row">
        <div class="col s12">

            <div>
                <h3>Login</h3>
            </div>

            <div class="row">
                <form class="col s12" role="form" method="POST" action="{{ url('/login') }}">
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="input-field col s12 {{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email">E-Mail Address</label>

                                <input id="email" type="email" class="validate" name="email">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s12 {{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password">Password</label>

                            <input id="password" type="password" class="validate" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field">
                            <div class="col s6">
                                <button class="btn waves-effect waves-light" type="submit" name="action">
                                    Submit
                                </button>

                                <a class="btn waves-effect waves-light"
                                   href="{{ url('/password/reset') }}">Forgot Your Password?</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="col-md-8 col-md-offset-2">
        </div>
    </div>

    </div>
@endsection
