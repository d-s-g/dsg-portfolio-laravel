@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col s12">

            <div>
                <h3>Register</h3>
            </div>

            <div class="row">
                <form class="col s12" role="form" method="POST" action="{{ /* url('/register') */ }}">
                {{ csrf_field() }}

                <div class="row">
                    <div class="input-field {{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name">Name</label>

                        <input id="name" type="text" class="validate" name="name">

                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif

                    </div>
                </div>

                <div class="row">
                    <div class="input-field {{ $errors->has('email') ? ' has-error' : '' }}">
                        <label for="email">E-Mail Address</label>

                        <input id="email" type="email" class="validate" name="email">

                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif

                    </div>
                </div>

                <div class="row">
                    <div class="input-field {{ $errors->has('password') ? ' has-error' : '' }}">
                        <label for="password">Password</label>

                        <input id="password" type="password" class="validate" name="password">

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif

                    </div>
                </div>

                <div class="row">
                    <div class="input-field {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <label for="password-confirm">Confirm Password</label>

                        <input id="password-confirm" type="password" class="validate" name="password_confirmation">

                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                        @endif

                    </div>
                </div>

                <div class="row">
                    <div class="input-field">
                        <button class="btn waves-effect waves-light" type="submit" name="action">
                            Submit
                        </button>
                    </div>
                </div>

            </form>

        </div>
    </div>
</div>
@endsection
