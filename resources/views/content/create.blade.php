@extends('layouts.layout')


@section('content')
    <form method="POST" action="/content/{{ $singleProject->slug }}/store">
       <div class="row">
           <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
           <div class="input-field">
               <label type="text" for="image">Image</label>
               <input name="image"></input>
           </div>
           <div class="input-field">
               <label for="description">Description</label>
               <textarea type="text" name="description" required></textarea>
           </div>
            <button class="waves-effect waves-light btn">Submit</button>
       </div>
    </form>
@stop