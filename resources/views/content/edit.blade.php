@extends('layouts.layout')

@section('content')
    <h1> Edit the content</h1>

    <form method="POST" action="/content/{{ $content->id }}">

        {{ method_field("PATCH") }}

        <div class="row">
            <input name="_token" type="hidden" value="{!! csrf_token() !!}" />
            <div class="input-field">
                <label type="text" for="image">{{ $content->image }}</label>
                <input name="image"></input>
            </div>
            <div class="input-field">
                <label for="description">{{ $content->description }}</label>
                <textarea type="text" name="description"></textarea>
            </div>
            <button class="waves-effect waves-light btn">Update</button>
        </div>
    </form>
@stop